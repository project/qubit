<?php

use Drupal\qubit\QubitUniversalVariable;

/**
 * @file
 * Theme the Qubit Opentag and Universal Variable.
 *
 * @var string $opentag_url
 *  The URL for the Opentag container external JavaScript library.
 * @var QubitUniversalVariable|null $universal_variable
 *  The Qubit Universal Variable object.
 * @var string|null $universal_variable_json
 *  The JSON encoded Qubit Universal Variable.
 * @var boolean $opentag_enabled
 *  The Tealium environment [dev|qa|prod].
 *
 * @link http://docs.qubitproducts.com/uv/implement/
 *
 * @see template_preprocess_qubit
 *
 * @ingroup themeable
 */
?>
<?php if ($opentag_url) : ?>
<!-- Begin Qubit Implementation -->
<?php if (!empty($universal_variable_json)) : ?>

  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    window.universal_variable = <?php print $universal_variable_json; ?>;
    //--><!]]>
  </script>
<?php endif; ?>
<?php if ($opentag_enabled) : ?>

  <script type="text/javascript" src="<?php print $opentag_url; ?>" async defer></script>
<?php endif; ?>

<!-- End Qubit Implementation -->
<?php endif; ?>
