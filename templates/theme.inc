<?php
/**
 * @file
 * Theme functions and preprocess hook implementations for Qubit templates.
 */

use Drupal\qubit\QubitUniversalVariable;

/**
 * Implements template_preprocess_HOOK().
 *
 * @param array $variables
 *   Reference to the template variables array keyed by variable name.
 *   The following keys are expected:
 *   - 'opentag_url' <string>
 *   - 'opentag_enabled' <boolean>
 *   - 'universal_variable' <QubitUniversalVariable|null>
 * @param string $hook
 *
 * @link http://docs.qubitproducts.com/uv/implement/
 *
 * @ingroup themeable
 */
function template_preprocess_qubit(array &$variables, $hook) {
  $variables['universal_variable_json'] = NULL;
  if (!array_key_exists('universal_variable', $variables)) {
    $variables['universal_variable'] = NULL;

    return;
  }

  $universal_variable = $variables['universal_variable'];

  if ($universal_variable instanceof QubitUniversalVariable) {
    $variables['universal_variable_json'] = json_encode(
      $universal_variable->jsonSerialize()
    );
  }
  else {
    $variables['universal_variable'] = NULL;
  }
}
