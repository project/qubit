<?php
/**
 * @file
 * Administration settings for Qubit.
 */


/**
 * @return array
 *  Drupal form API array
 */
function qubit_admin_form() {
  $form = array();

  $form['qubit']['qubit_opentag_url'] = array(
    '#type' => 'textfield',
    '#title' => t('OpenTag URL'),
    '#default_value' => variable_get(
      'qubit_opentag_url', QUBIT_DEFAULT_OPENTAG
    ),
    '#size' => 80,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#element_validate' => array('qubit_opentag_url_validate'),
  );

  $form['qubit']['qubit_opentag_enable'] = array(
    '#type' => 'radios',
    '#title' => t('Output the Opentag container'),
    '#description' => t(
      'When output of the Qubit Opentag container is disabled the Universal Variable will still be populated but it will not be sent to Qubit.'
    ),
    '#default_value' => variable_get('qubit_opentag_enable', 1),
    '#options' => array(
      0 => t('Disable'),
      1 => t('Enable'),
    ),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
