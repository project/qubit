<?php
/**
 * @file
 * Interface for Abstract Qubit Universal Variable
 */

namespace Drupal\qubit;

/**
 * @package Drupal\qubit
 */
interface AbstractUniversalVariableInterface extends \IteratorAggregate {

  /**
   * Return the object properties as a JSON encoded string
   *
   * @return string
   */
  public function __toString();

  /**
   * Serializes the object to a value that can be serialized natively
   * by json_encode().
   *
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   *
   * @return mixed
   *  Returns data which can be serialized by json_encode().
   */
  public function jsonSerialize();

}
