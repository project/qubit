<?php
/**
 * @file
 * Abstract Qubit Universal Variable
 */

namespace Drupal\qubit;

use DateTime;
use InvalidArgumentException;

/**
 * Class AbstractUniversalVariable
 *
 * An abstract class to enable a Qubit Universal Variable object to:
 * - be serialized and encoded as a JSON string
 * - have its set and unset properties traversed
 * - allow its properties to be unset
 *
 * @package Drupal\qubit
 */
abstract class AbstractUniversalVariable implements AbstractUniversalVariableInterface {

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  abstract protected function getSetProperties();

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  abstract protected function getAllProperties();

  /**
   * Unset a property
   *
   * @param string $name
   * @return void
   */
  abstract public function __unset($name);

  /**
   * Return the object properties as a JSON encoded string
   *
   * @return string
   */
  public function __toString() {
    $serialized = $this->jsonSerialize();

    $json = json_encode(
      $serialized,
      JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT
    );

    return $json;
  }

  /**
   * Retrieve an external iterator
   *
   * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
   * @return \ArrayIterator
   *  An instance of an object implementing \Traversable
   */
  public function getIterator() {
    $properties = $this->getAllProperties();
    $iterator = new \ArrayIterator($properties);

    return $iterator;
  }

  /**
   * Get a Valid Price string
   *
   * @param mixed $value
   *  Numeric value for use as a price.
   * @return string
   * @throws \InvalidArgumentException
   */
  public static function validateNumericPrice($value) {
    if (!is_numeric($value)) {
      throw new InvalidArgumentException("Unexpected type '" . gettype($value) . "' given.");
    }
    $float = floatval($value);
    $number = number_format($float, 2);

    return $number;
  }

  /**
   * Serializes the object to a value that can be serialized natively
   * by json_encode().
   *
   * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
   *
   * @return mixed
   *  Returns data which can be serialized by json_encode().
   */
  public function jsonSerialize() {
    $properties = $this->getSetProperties();

    $serialized = array();

    foreach ($properties as $name => $value) {
      $json_value = $this->jsonSerializeProperty($value);

      if (isset($json_value)) {
        $serialized[$name] = $json_value;
      }
    }

    if (count($serialized) > 0) {

      return (object) $serialized;
    }
    else {

      return new \stdClass;
    }
  }

  /**
   * Prepare a Property Value for encoding as JSON
   *
   * @param $value
   * @return null|object|string
   */
  private function jsonSerializeProperty($value) {
    if (is_array($value)) {
      return $this->jsonSerializeArray($value);
    }
    elseif ($value instanceof DateTime) {
      $json_value = $value->format(DateTime::ISO8601);

      return $json_value;
    }
    elseif ($value instanceof AbstractUniversalVariable) {
      return $value->jsonSerialize();
    }
    else {
      $json_value = $value;

      return $json_value;
    }
  }

  /**
   * @param array $array
   */
  private function jsonSerializeArray(array $array) {
    $serialized = array();

    foreach ($array as $key => $value) {
      $serialized[$key] = $this->jsonSerializeProperty($value);
    }

    return $serialized;
  }

}