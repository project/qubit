<?php
/**
 * @file
 * Universal Variable Event
 */

namespace Drupal\qubit\UniversalVariable;


use DateTime;
use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitEvent
 *
 * The Event object identifies when something has just happened, either
 * since the last page view, or during the current page view.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#event
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitEvent extends AbstractUniversalVariable implements QubitEventInterface {

  /** @var string */
  private $type;
  /** @var DateTime */
  private $time;
  /** @var string */
  private $cause;
  /** @var string */
  private $effect;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $cause
   * @return $this
   */
  public function setCause($cause) {
    $this->cause = $cause;

    return $this;
  }

  /**
   * @return string
   */
  public function getCause() {
    return $this->cause;
  }

  /**
   * @param string $effect
   * @return $this
   */
  public function setEffect($effect) {
    $this->effect = $effect;

    return $this;
  }

  /**
   * @return string
   */
  public function getEffect() {
    return $this->effect;
  }

  /**
   * @param \DateTime $time
   * @return $this
   */
  public function setTime(DateTime $time) {
    $this->time = $time;

    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getTime() {
    return $this->time;
  }

  /**
   * @param string $type
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;

    return $this;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

}