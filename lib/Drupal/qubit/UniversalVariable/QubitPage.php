<?php
/**
 * @file
 * Universal Variable Page
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitPage
 *
 * The Page object describes the current page.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#page
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitPage extends AbstractUniversalVariable implements QubitPageInterface {

  /** @var string */
  private $category;
  /** @var string */
  private $subcategory;
  /** @var string */
  private $environment;
  /** @var string */
  private $variation;
  /** @var string */
  private $revision;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $category
   * @return $this
   */
  public function setCategory($category) {
    $this->category = $category;

    return $this;
  }

  /**
   * @return string
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @param string $environment
   *  The system environment creating this page,
   *  e.g. 'development', 'testing', 'production'
   * @return $this
   */
  public function setEnvironment($environment) {
    $this->environment = $environment;

    return $this;
  }

  /**
   * @return string
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * @param string $revision
   * @return $this
   */
  public function setRevision($revision) {
    $this->revision = $revision;

    return $this;
  }

  /**
   * @return string
   */
  public function getRevision() {
    return $this->revision;
  }

  /**
   * @param string $subcategory
   * @return $this
   */
  public function setSubcategory($subcategory) {
    $this->subcategory = $subcategory;

    return $this;
  }

  /**
   * @return string
   */
  public function getSubcategory() {
    return $this->subcategory;
  }

  /**
   * @param string $variation
   *  If serving multiple versions of this page during testing,
   *  specify a variation name. e.g. 'original','new'
   * @return $this
   */
  public function setVariation($variation) {
    $this->variation = $variation;

    return $this;
  }

  /**
   * @return string
   */
  public function getVariation() {
    return $this->variation;
  }

}