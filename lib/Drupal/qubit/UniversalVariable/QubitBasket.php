<?php
/**
 * @file
 * Universal Variable Basket
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitBasket
 *
 * The Basket object describes the current state of the a user's shopping basket or cart.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#basket
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitBasket extends AbstractUniversalVariable implements QubitBasketInterface {

  /** @var string */
  private $id;
  /** @var string */
  private $currency;
  /** @var float */
  private $subtotal;
  /** @var boolean */
  private $subtotal_include_tax;
  /** @var string */
  private $voucher;
  /** @var float */
  private $voucher_discount;
  /** @var float */
  private $tax;
  /** @var float */
  private $shipping_cost;
  /** @var string */
  private $shipping_method;
  /** @var float */
  private $total;
  /** @var array */
  private $line_items;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param mixed $currency
   * @return $this
   */
  public function setCurrency($currency) {
    $this->currency = $currency;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @param string $id
   * @return $this
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param array $line_items
   * @return $this
   */
  public function setLineItems($line_items) {
    $this->line_items = $line_items;

    return $this;
  }

  /**
   * @return array
   */
  public function getLineItems() {
    return $this->line_items;
  }

  /**
   * @param float $shipping_cost
   * @return $this
   */
  public function setShippingCost($shipping_cost) {
    $this->shipping_cost = $shipping_cost;

    return $this;
  }

  /**
   * @return float
   */
  public function getShippingCost() {
    return $this->shipping_cost;
  }

  /**
   * @param string $shipping_method
   * @return $this
   */
  public function setShippingMethod($shipping_method) {
    $this->shipping_method = $shipping_method;

    return $this;
  }

  /**
   * @return string
   */
  public function getShippingMethod() {
    return $this->shipping_method;
  }

  /**
   * @param float $subtotal
   * @return $this
   */
  public function setSubtotal($subtotal) {
    $this->subtotal = $subtotal;

    return $this;
  }

  /**
   * @return float
   */
  public function getSubtotal() {
    return $this->subtotal;
  }

  /**
   * @param boolean $subtotal_include_tax
   * @return $this
   */
  public function setSubtotalIncludeTax($subtotal_include_tax) {
    $this->subtotal_include_tax = $subtotal_include_tax;

    return $this;
  }

  /**
   * @return boolean
   */
  public function getSubtotalIncludeTax() {
    return $this->subtotal_include_tax;
  }

  /**
   * @param float $tax
   * @return $this
   */
  public function setTax($tax) {
    $this->tax = $tax;

    return $this;
  }

  /**
   * @return float
   */
  public function getTax() {
    return $this->tax;
  }

  /**
   * @param float $total
   * @return $this
   */
  public function setTotal($total) {
    $this->total = $total;

    return $this;
  }

  /**
   * @return float
   */
  public function getTotal() {
    return $this->total;
  }

  /**
   * @param string $voucher
   * @return $this
   */
  public function setVoucher($voucher) {
    $this->voucher = $voucher;

    return $this;
  }

  /**
   * @return string
   */
  public function getVoucher() {
    return $this->voucher;
  }

  /**
   * @param float $voucher_discount
   * @return $this
   */
  public function setVoucherDiscount($voucher_discount) {
    $this->voucher_discount = $voucher_discount;

    return $this;
  }

  /**
   * @return float
   */
  public function getVoucherDiscount() {
    return $this->voucher_discount;
  }



}