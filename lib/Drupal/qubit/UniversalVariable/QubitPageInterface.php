<?php
/**
 * @file
 * Interface for Universal Variable Page
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;


interface QubitPageInterface {

  public function setCategory($category);

  public function getCategory();

  /**
   * @param string $environment
   *  The system environment creating this page,
   *  e.g. 'development', 'testing', 'production'
   */
  public function setEnvironment($environment);

  public function getEnvironment();

  public function setRevision($revision);

  public function getRevision();

  public function setSubcategory($subcategory);

  public function getSubcategory();

  /**
   * @param string $variation
   *  If serving multiple versions of this page during testing,
   *  specify a variation name. e.g. 'original','new'
   * @return $this
   */
  public function setVariation($variation);

  public function getVariation();

}