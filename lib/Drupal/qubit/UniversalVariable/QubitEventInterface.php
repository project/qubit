<?php
/**
 * @file
 * Interface for the Universal Variable Event
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;


use DateTime;

interface QubitEventInterface {

  public function setCause($cause);

  public function getCause();

  public function setEffect($effect);

  public function getEffect();

  public function setTime(DateTime $time);

  public function getTime();

  public function setType($type);

  public function getType();

}