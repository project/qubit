<?php
/**
 * @file
 * Interface for the Universal Variable Transaction
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\UniversalVariable\Transaction\QubitAddress;
use Drupal\qubit\UniversalVariable\Transaction\QubitLineItem;

interface QubitTransactionInterface {

  public function setOrderId($order_id);

  public function getOrderId();

  public function setCurrency($currency);

  public function getCurrency();

  /**
   * @param string $payment_type
   *  Payment method, e.g. 'Visa', 'PayPal', 'Voucher'
   * @return $this
   */
  public function setPaymentType($payment_type);

  /**
   * @return string
   *  Payment method, e.g. 'Visa', 'PayPal', 'Voucher'
   */
  public function getPaymentType();

  /**
   * @param float $subtotal
   *  The Transaction Price, excluding shipping or discounts.
   * @return float
   */
  public function setSubtotal($subtotal);

  /**
   * @return float
   *  The Transaction Price, excluding shipping or discounts.
   */
  public function getSubtotal();

  /**
   * @param boolean $subtotal_include_tax
   *  Whether Transaction Price includes tax.
   * @return $this
   */
  public function setSubtotalIncludeTax($subtotal_include_tax);

  /**
   * @return boolean
   *  Whether Transaction Price includes tax.
   */
  public function getSubtotalIncludeTax();

    /**
   * Set Transaction Returning Status
   *
   * @param boolean $hasReturned
   *  False if this is the first time a user has been served this Transaction,
   *  True if this Transaction has happened some time ago and its details are being reviewed.
   * @return $this
   */
  public function setHasReturned($hasReturned);

  /**
   * Get Transaction Returning Status
   *
   * @return boolean|null
   *  False if this is the first time a user has been served this Transaction,
   *  True if this Transaction has happened some time ago and its details are being reviewed.
   *  Null if not set.
   */
  public function getHasReturned();

  public function setDeliveryAddress(QubitAddress $delivery);

  public function getDeliveryAddress();

  public function setBillingAddress(QubitAddress $billing);

  public function getBillingAddress();

  public function setLineItems(array $line_items);

  public function getLineItems();

}