<?php
/**
 * @file
 * Universal Variable Transaction Line Item
 */

namespace Drupal\qubit\UniversalVariable\Transaction;


use Drupal\qubit\AbstractUniversalVariable;
use Drupal\qubit\UniversalVariable\QubitProduct;

/**
 * Class QubitLineItem
 *
 * The LineItem object describes a quantity of Products.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#lineitem
 *
 * @package Drupal\qubit\QubitUniversalVariable\Transaction
 */
class QubitLineItem extends AbstractUniversalVariable implements QubitLineItemInterface {

  /** @var QubitProduct */
  private $product;
  /** @var float */
  private $quantity;
  /** @var float */
  private $subtotal;
  /** @var float */
  private $total_discount;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param QubitProduct $product
   * @return $this
   */
  public function setProduct(QubitProduct $product) {
    $this->product = $product;

    return $this;
  }

  /**
   * @return QubitProduct
   */
  public function getProduct() {
    return $this->product;
  }

  /**
   * @param mixed $quantity
   * @return $this
   */
  public function setQuantity($quantity) {
    $this->quantity = $quantity;

    return $this;
  }

  /**
   * @return float
   */
  public function getQuantity() {
    return $this->quantity;
  }

  /**
   * @param mixed $subtotal
   * @return $this
   */
  public function setSubtotal($subtotal) {
    $this->subtotal = $subtotal;

    return $this;
  }

  /**
   * @return float
   */
  public function getSubtotal() {
    return $this->subtotal;
  }

  /**
   * @param mixed $total_discount
   * @return $this
   */
  public function setTotalDiscount($total_discount) {
    $this->total_discount = $total_discount;

    return $this;
  }

  /**
   * @return float
   */
  public function getTotalDiscount() {
    return $this->total_discount;
  }

}