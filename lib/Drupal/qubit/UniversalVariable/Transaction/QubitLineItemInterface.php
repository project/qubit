<?php
/**
 * @file
 * Interface for the Universal Variable Transaction Line Item
 * 
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable\Transaction;


use Drupal\qubit\UniversalVariable\QubitProduct;

interface QubitLineItemInterface {

  public function setProduct(QubitProduct $product);

  public function getProduct();

  public function setQuantity($quantity);

  public function getQuantity();

}