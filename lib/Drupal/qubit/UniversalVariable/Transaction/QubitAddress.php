<?php
/**
 * @file
 * Universal Variable Transaction Address
 */

namespace Drupal\qubit\UniversalVariable\Transaction;


use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitAddress
 *
 * The Address object is used for billing and shipping information
 * in the Transaction object.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#address
 *
 * @package Drupal\qubit\QubitUniversalVariable\Transaction
 */
class QubitAddress extends AbstractUniversalVariable implements QubitAddressInterface {

  /** @var string */
  private $name;
  /** @var string */
  private $address;
  /** @var string */
  private $city;
  /** @var string */
  private $state;
  /** @var string */
  private $postcode;
  /** @var string */
  private $country;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $address
   * @return $this
   */
  public function setAddress($address) {
    $this->address = $address;

    return $this;
  }

  /**
   * @return string
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @param string $city
   * @return $this
   */
  public function setCity($city) {
    $this->city = $city;

    return $this;
  }

  /**
   * @return string
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @param string $country
   * @return $this
   */
  public function setCountry($country) {
    $this->country = $country;

    return $this;
  }

  /**
   * @return string
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $postcode
   * @return $this
   */
  public function setPostcode($postcode) {
    $this->postcode = $postcode;

    return $this;
  }

  /**
   * @return string
   */
  public function getPostcode() {
    return $this->postcode;
  }

  /**
   * @param string $state
   * @return $this
   */
  public function setState($state) {
    $this->state = $state;

    return $this;
  }

  /**
   * @return string
   */
  public function getState() {
    return $this->state;
  }

}