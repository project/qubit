<?php
/**
 * @file
 * Universal Variable Product
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\AbstractUniversalVariable;
use Drupal\qubit\UniversalVariable\Product\QubitReview;
use Drupal\qubit\UniversalVariable\Product\QubitJourney;
use Drupal\qubit\UniversalVariable\Product\QubitAccommodation;
use InvalidArgumentException;

/**
 * Class QubitProduct
 *
 * The Product object describes a single product with optional properties.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#product
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitProduct extends AbstractUniversalVariable implements QubitProductInterface {

  /** @var string */
  private $id;
  /** @var string */
  private $url;
  /** @var string */
  private $sku_code;
  /** @var string */
  private $name;
  /** @var string */
  private $description;
  /** @var string */
  private $manufacturer;
  /** @var string */
  private $category;
  /** @var string */
  private $subcategory;

  /** @var QubitProduct[] */
  private $linked_products;

  /** @var string */
  private $currency;
  /** @var string */
  private $unit_sale_price;
  /** @var string */
  private $unit_price;

  /** @var QubitReview[] */
  private $reviews;

  /** @var integer */
  private $stock;

  /** @var string */
  private $voucher;

  /** @var string */
  private $color;
  /** @var string */
  private $size;

  /** @var QubitJourney[] */
  private $journeys;
  /** @var QubitAccommodation[] */
  private $accommodations;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param QubitAccommodation[] $accommodations
   * @return $this
   */
  public function setAccommodations(array $accommodations) {
    $this->accommodations = $accommodations;

    return $this;
  }

  /**
   * @return QubitAccommodation[]|null
   */
  public function getAccommodations() {
    return $this->accommodations;
  }

  /**
   * @param string $category
   * @return $this
   */
  public function setCategory($category) {
    $this->category = $category;

    return $this;
  }

  /**
   * @return string
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * @param string $color
   * @return $this
   */
  public function setColor($color) {
    $this->color = $color;

    return $this;
  }

  /**
   * @return string
   */
  public function getColor() {
    return $this->color;
  }

  /**
   * @param string $currency
   * @return $this
   */
  public function setCurrency($currency) {
    $this->currency = $currency;

    return $this;
  }

  /**
   * @return string
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @param string $description
   * @return $this
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $id
   * @return $this
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param QubitJourney[] $journeys
   * @return $this
   */
  public function setJourneys(array $journeys) {
    $this->journeys = $journeys;

    return $this;
  }

  /**
   * @return QubitJourney[]|null
   */
  public function getJourneys() {
    return $this->journeys;
  }

  /**
   * @param string $manufacturer
   * @return $this
   */
  public function setManufacturer($manufacturer) {
    $this->manufacturer = $manufacturer;

    return $this;
  }

  /**
   * @return string
   */
  public function getManufacturer() {
    return $this->manufacturer;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param QubitReview[] $reviews
   * @return $this
   */
  public function setReviews(array $reviews) {
    $this->reviews = $reviews;

    return $this;
  }

  /**
   * @return QubitReview[]|null
   */
  public function getReviews() {
    return $this->reviews;
  }

  /**
   * @param string $size
   * @return $this
   */
  public function setSize($size) {
    $this->size = $size;

    return $this;
  }

  /**
   * @return string
   */
  public function getSize() {
    return $this->size;
  }

  /**
   * @param string $sku_code
   * @return $this
   */
  public function setSkuCode($sku_code) {
    $this->sku_code = $sku_code;

    return $this;
  }

  /**
   * @return string
   */
  public function getSkuCode() {
    return $this->sku_code;
  }

  /**
   * @param mixed $stock
   *  The numeric quantity of stock remaining
   * @return $this
   */
  public function setStock($stock) {
    if (!is_numeric($stock)) {
      throw new InvalidArgumentException('Cannot set Stock to a non-numeric value.');
    }
    $this->stock = $stock;

    return $this;
  }

  /**
   * @return mixed
   *  The numeric quantity of stock remaining else NULL.
   */
  public function getStock() {
    return $this->stock;
  }

  /**
   * @param string $subcategory
   * @return $this
   */
  public function setSubcategory($subcategory) {
    $this->subcategory = $subcategory;

    return $this;
  }

  /**
   * @return string
   */
  public function getSubcategory() {
    return $this->subcategory;
  }

  /**
   * @param string $url
   * @return $this
   */
  public function setUrl($url) {
    $this->url = $url;

    return $this;
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * @param string $voucher
   * @return $this
   */
  public function setVoucher($voucher) {
    $this->voucher = $voucher;

    return $this;
  }

  /**
   * @return string
   */
  public function getVoucher() {
    return $this->voucher;
  }

  /**
   * @param QubitProduct[] $linked_products
   * @return $this
   */
  public function setLinkedProducts(array $linked_products) {
    $this->linked_products = $linked_products;

    return $this;
  }

  /**
   * @return QubitProduct[]|null
   */
  public function getLinkedProducts() {
    return $this->linked_products;
  }

  /**
   * @param float $unit_price
   *  The price of a single unit of this product, not taking into
   *  account discounts and promotions.
   *  <em>Requires Product Currency and Product Price to be
   *  declared.</em>
   * @return $this
   */
  public function setUnitPrice($price) {
    $this->unit_price = $this->validateNumericPrice($price);

    return $this;
  }

  /**
   * @return float
   */
  public function getUnitPrice() {
    return $this->unit_price;
  }

  /**
   * @param float $unit_sale_price
   *  The price for a single unit of this product actually paid by a
   *  customer, taking into account any sales and promotions.
   *  <em>Requires Product Currency to be declared.</em>
   * @return $this
   */
  public function setUnitSalePrice($price) {
    $this->unit_sale_price = $this->validateNumericPrice($price);

    return $this;
  }

  /**
   * @return string
   */
  public function getUnitSalePrice() {
    return $this->unit_sale_price;
  }

}