<?php
/**
 * @file
 * Universal Variable Recommendation
 */

namespace Drupal\qubit\UniversalVariable;

use Drupal\qubit\AbstractUniversalVariable;
use InvalidArgumentException;

/**
 * Class QubitRecommendation
 *
 * The Recommendation object describes products that have been
 * recommended on a page, based on recommendation algorithms.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#recommendation
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitRecommendation extends AbstractUniversalVariable implements QubitRecommendationInterface {

  private $items;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param QubitProduct[] $items
   *  The products which have been recommended to the user on this page.
   * @throws InvalidArgumentException
   * @return $this
   */
  public function setItems(array $items) {
    foreach ($items as $item) {
      if (FALSE === $item instanceof QubitProduct) {
        throw new InvalidArgumentException("Type Error: items can only contain instances of class 'Product'.");
      }
    }

    $this->items = $items;

    return $this;
  }

  /**
   * @return QubitProduct[]|null
   *  The products which have been recommended to the user on this page.
   */
  public function getItems() {
    return $this->items;
  }

}