<?php
/**
 * @file
 * Interface for the Universal Variable User
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;


interface QubitUserInterface {

  public function setRealName($realName);

  public function getRealName();

  public function setLoginName($loginName);

  public function getLoginName();

  public function setUserId($id);

  public function getUserId();

  public function setEmail($email);

  public function getEmail();

  public function setLanguage($language);

  public function getLanguage();

  public function setHasReturned($hasReturned);

  public function getHasReturned();

  public function setHasTransacted($hasTransacted);

  public function getHasTransacted();

  public function setTypes(array $types);

  public function getTypes();

  public function setFacebookId($id);

  public function getFacebookId();

  public function setTwitterId($id);

  public function getTwitterId();

}