<?php
/**
 * @file
 * Interface for the Universal Variable Product Journey
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable\Product;


interface QubitJourneyInterface {

  public function setPaxCountAdult($paxCountAdult);

  public function getPaxCountAdult();

  public function setPaxCountChild($paxCountChild);

  public function getPaxCountChild();

  public function setPaxCountInfant($paxCountInfant);

  public function getPaxCountInfant();

}