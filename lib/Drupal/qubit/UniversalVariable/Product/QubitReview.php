<?php
/**
 * @file
 * Universal Variable Product Review
 */

namespace Drupal\qubit\UniversalVariable\Product;


use Drupal\qubit\AbstractUniversalVariable;
use InvalidArgumentException;

/**
 * Class QubitReview
 *
 * The Review object models a review of a Product.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#review
 *
 * @package Drupal\qubit\QubitUniversalVariable\Product
 */
class QubitReview extends AbstractUniversalVariable implements QubitReviewInterface {

  /** @var string */
  private $body;

  /** @var float|null */
  private $rating = NULL;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $body
   * @return $this
   */
  public function setBody($body) {
    $this->body = $body;

    return $this;
  }

  /**
   * @return string
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * @param mixed $rating
   *  A numeric rating or NULL to unset.
   * @throws InvalidArgumentException
   * @return $this
   */
  public function setRating($rating) {
    if (!is_numeric($rating) && !is_null($rating)) {
      throw new InvalidArgumentException('Error: Cannot set rating to a non-numeric value.');
    }

    $this->rating = floatval($rating);

    return $this;
  }

  /**
   * @return mixed
   *  A numeric rating else NULL.
   */
  public function getRating() {
    return $this->rating;
  }


}