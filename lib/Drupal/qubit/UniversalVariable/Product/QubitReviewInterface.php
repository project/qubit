<?php
/**
 * @file
 * Interface for the Universal Variable Product Review
 * 
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable\Product;


interface QubitReviewInterface {

  public function setBody($body);

  public function getBody();

  public function setRating($rating);

  public function getRating();

}