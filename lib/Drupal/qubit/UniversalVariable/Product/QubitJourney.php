<?php
/**
 * @file
 * Universal Variable Product Journey
 */

namespace Drupal\qubit\UniversalVariable\Product;

use DateTime;
use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitJourney
 *
 * The Journey object is used as part of a travel-related Product,
 * representing a single 'leg' of travel.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#journey
 *
 * @package Drupal\qubit\QubitUniversalVariable\Product
 */
class QubitJourney extends AbstractUniversalVariable implements QubitJourneyInterface {

  /** @var string */
  private $type;
  /** @var string */
  private $name;
  /** @var string */
  private $code;
  /** @var DateTime */
  private $time;
  /** @var integer */
  private $paxCountAdult;
  /** @var integer */
  private $paxCountChild;
  /** @var integer */
  private $paxCountInfant;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $code
   * @return $this
   */
  public function setCode($code) {
    $this->code = $code;

    return $this;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param int $paxCountAdult
   * @return $this
   */
  public function setPaxCountAdult($paxCountAdult) {
    $this->paxCountAdult = $paxCountAdult;

    return $this;
  }

  /**
   * @return int
   */
  public function getPaxCountAdult() {
    return $this->paxCountAdult;
  }

  /**
   * @param int $paxCountChild
   * @return $this
   */
  public function setPaxCountChild($paxCountChild) {
    $this->paxCountChild = $paxCountChild;

    return $this;
  }

  /**
   * @return int
   */
  public function getPaxCountChild() {
    return $this->paxCountChild;
  }

  /**
   * @param int $paxCountInfant
   * @return $this
   */
  public function setPaxCountInfant($paxCountInfant) {
    $this->paxCountInfant = $paxCountInfant;

    return $this;
  }

  /**
   * @return int
   */
  public function getPaxCountInfant() {
    return $this->paxCountInfant;
  }

  /**
   * @param \DateTime $time
   * @return $this
   */
  public function setTime($time) {
    $this->time = $time;

    return $this;
  }

  /**
   * @return \DateTime
   */
  public function getTime() {
    return $this->time;
  }

  /**
   * @param string $type
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;

    return $this;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

}