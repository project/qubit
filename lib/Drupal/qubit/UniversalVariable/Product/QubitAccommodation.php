<?php
/**
 * @file
 * Universal Variable Product Accommodation
 */

namespace Drupal\qubit\UniversalVariable\Product;


use DateTime;
use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitAccommodation
 *
 * The Accommodation object is used as part of a travel-related Product.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#accommodation
 *
 * @package Drupal\qubit\QubitUniversalVariable\Product
 */
class QubitAccommodation extends AbstractUniversalVariable implements QubitAccommodationInterface {

  /** @var string */
  private $type;
  /** @var string */
  private $name;
  /** @var string */
  private $code;

  /** @var DateTime */
  private $checkin_time;
  /** @var DateTime */
  private $checkout_time;

  /** @var integer */
  private $adults;
  /** @var integer */
  private $children;
  /** @var integer */
  private $infants;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param int $adults
   * @return $this
   */
  public function setAdults($adults) {
    $this->adults = $adults;

    return $this;
  }

  /**
   * @return int
   */
  public function getAdults() {
    return $this->adults;
  }

  /**
   * @param DateTime $checkin_time
   * @return $this
   */
  public function setCheckinTime(DateTime $checkin_time) {
    $this->checkin_time = $checkin_time;

    return $this;
  }

  /**
   * @return DateTime
   */
  public function getCheckinTime() {
    return $this->checkin_time;
  }

  /**
   * @param DateTime $checkout_time
   * @return $this
   */
  public function setCheckoutTime(DateTime $checkout_time) {
    $this->checkout_time = $checkout_time;

    return $this;
  }

  /**
   * @return DateTime
   */
  public function getCheckoutTime() {
    return $this->checkout_time;
  }

  /**
   * @param int $children
   * @return $this
   */
  public function setChildren($children) {
    $this->children = $children;

    return $this;
  }

  /**
   * @return int
   */
  public function getChildren() {
    return $this->children;
  }

  /**
   * @param string $code
   * @return $this
   */
  public function setCode($code) {
    $this->code = $code;

    return $this;
  }

  /**
   * @return string
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @param int $infants
   * @return $this
   */
  public function setInfants($infants) {
    $this->infants = $infants;

    return $this;
  }

  /**
   * @return int
   */
  public function getInfants() {
    return $this->infants;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $type
   * @return $this
   */
  public function setType($type) {
    $this->type = $type;

    return $this;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

}