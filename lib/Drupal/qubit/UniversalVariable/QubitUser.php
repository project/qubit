<?php
/**
 * @file
 * Universal Variable User
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\AbstractUniversalVariable;

/**
 * Class QubitUser
 *
 * The User object describes the current user of a web site.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#user
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitUser extends AbstractUniversalVariable implements QubitUserInterface {

  /** @var  string */
  private $name;
  /** @var  string */
  private $username;
  /** @var  string */
  private $user_id;
  /** @var  string */
  private $email;
  /** @var  string */
  private $language;

  /** @var  boolean */
  private $returning;
  /** @var  boolean */
  private $has_transacted;
  /** @var  array */
  private $types;

  /** @var string */
  private $facebook_id;
  /** @var string */
  private $twitter_id;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});

      return;
    }

    switch ($name) {
      case 'real_name':
        unset($this->name);
        break;

      case 'login_name':
        unset($this->username);
        break;

      case 'has_returned':
        unset($this->returning);
        break;
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param string $realName
   *  The user's full name.
   * @return $this
   */
  public function setRealName($realName) {
    $this->name = $realName;

    return $this;
  }

  /**
   * @return string
   *  The user's full name.
   */
  public function getRealName() {
    return $this->name;
  }

  /**
   * @param string $loginName
   *  The identifier that the user provides to log in to the site
   *  (the 'username').
   * @return $this
   */
  public function setLoginName($loginName) {
    $this->username = $loginName;

    return $this;
  }

  /**
   * @return string
   *  The identifier that the user provides to log in to the site
   *  (the 'username').
   */
  public function getLoginName() {
    return $this->username;
  }

  /**
   * @param string $id
   * @return $this
   */
  public function setUserId($id) {
    $this->user_id = $id;

    return $this;
  }

  /**
   * @return string
   */
  public function getUserId() {
    return $this->user_id;
  }

  /**
   * @param string $email
   * @return $this
   */
  public function setEmail($email) {
    $this->email = $email;

    return $this;
  }

  /**
   * @return string
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param string $language
   * @return $this
   */
  public function setLanguage($language) {
    $this->language = $language;

    return $this;
  }

  /**
   * @return string
   */
  public function getLanguage() {
    return $this->language;
  }

  /**
   * @param boolean $hasReturned
   * @return $this
   */
  public function setHasReturned($hasReturned) {
    $this->returning = (boolean) $hasReturned;

    return $this;
  }

  /**
   * @return boolean
   */
  public function getHasReturned() {
    return $this->returning;
  }

  /**
   * @param boolean $hasTransacted
   * @return $this
   */
  public function setHasTransacted($hasTransacted) {
    $this->has_transacted = (boolean) $hasTransacted;

    return $this;
  }

  /**
   * @return boolean
   */
  public function getHasTransacted() {
    return $this->has_transacted;
  }

  /**
   * @param array $types
   * @return $this
   */
  public function setTypes(array $types) {
    $strings = array_map(function($value) { return strval($value); }, $types);

    $this->types = array_values($strings);

    return $this;
  }

  /**
   * @return array
   */
  public function getTypes() {
    return $this->types;
  }

  /**
   * @param string|int $facebookId
   *  The user's Facebook User ID, as returned by the Facebook API.
   *  <em>NOTE: Must be numeric.</em>
   * @throws \InvalidArgumentException
   * @return $this
   */
  public function setFacebookId($facebookId) {
    if (!is_numeric($facebookId)) {
      throw new \InvalidArgumentException('Error: Cannot set facebook id to a non-numeric value.');
    }
    $this->facebook_id = "$facebookId";

    return $this;
  }

  /**
   * @return string
   */
  public function getFacebookId() {
    return $this->facebook_id;
  }

  /**
   * @param string $twitterId
   * @return $this
   */
  public function setTwitterId($twitterId) {
    $this->twitter_id = $twitterId;

    return $this;
  }

  /**
   * @return string
   */
  public function getTwitterId() {
    return $this->twitter_id;
  }

}