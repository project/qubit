<?php
/**
 * @file
 * Interface for Universal Variable Recommendation
 * 
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;


interface QubitRecommendationInterface {

  /**
   * @param QubitProduct[] $items
   *  The products which have been recommended to the user on this page.
   * @throws \InvalidArgumentException
   * @return $this
   */
  public function setItems(array $items);

  /**
   * @return QubitProduct[]|null
   *  The products which have been recommended to the user on this page.
   */
  public function getItems();

  }