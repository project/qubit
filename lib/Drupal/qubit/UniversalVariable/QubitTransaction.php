<?php
/**
 * @file
 * Universal Variable Transaction
 */

namespace Drupal\qubit\UniversalVariable;


use Drupal\qubit\AbstractUniversalVariable;
use Drupal\qubit\UniversalVariable\Transaction\QubitAddress;
use Drupal\qubit\UniversalVariable\Transaction\QubitLineItem;

/**
 * Class QubitTransaction
 *
 * The Transaction object describes a completed purchase, and could
 * be displayed on a confirmation or receipt page.
 *
 * @link https://github.com/QubitProducts/UniversalVariable#transaction
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
class QubitTransaction extends AbstractUniversalVariable implements QubitTransactionInterface {

  /** @var string */
  private $order_id;
  /** @var boolean */
  private $returning;
  /** @var string */
  private $currency;
  /** @var string */
  private $payment_type;
  /** @var mixed */
  private $subtotal;
  /** @var boolean */
  private $subtotal_include_tax;
  /** @var string */
  private $voucher;
  /** @var string */
  private $voucher_discount;
  /** @var string */
  private $tax;
  /** @var string */
  private $shipping_cost;
  /** @var string */
  private $shipping_method;
  /** @var string */
  private $total;
  /** @var QubitAddress */
  private $delivery;
  /** @var QubitAddress */
  private $billing;
  /** @var array */
  private $line_items;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
    elseif ($name == 'has_returned'){
      unset($this->returning);
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param mixed $total
   *  Numeric total price.
   * @return $this
   */
  public function setTotal($total) {
    $this->total = $this->validateNumericPrice($total);

    return $this;
  }

  /**
   * @return string
   *  Numeric total price.
   */
  public function getTotal() {
    return $this->total;
  }

  /**
   * @param mixed $shipping_cost
   *  Numeric shipping cost price.
   * @return $this
   */
  public function setShippingCost($shipping_cost) {
    $this->shipping_cost = $this->validateNumericPrice($shipping_cost);

    return $this;
  }

  /**
   * @return string
   *  Numeric shipping cost price.
   */
  public function getShippingCost() {
    return $this->shipping_cost;
  }

  /**
   * @param mixed $tax
   *  Numeric tax price.
   * @return $this
   */
  public function setTax($tax) {
    $this->tax = $this->validateNumericPrice($tax);

    return $this;
  }

  /**
   * @return string
   *  Numeric tax price.
   */
  public function getTax() {
    return $this->tax;
  }

  /**
   * @param boolean $subtotal_has_tax
   *  Whether Transaction Price includes tax.
   * @return $this
   */
  public function setSubtotalIncludeTax($subtotal_has_tax) {
    $this->subtotal_include_tax = (boolean) $subtotal_has_tax;

    return $this;
  }

  /**
   * @return boolean
   *  Whether Transaction Price includes tax.
   */
  public function getSubtotalIncludeTax() {
    return $this->subtotal_include_tax;
  }

  /**
   * Set Transaction Returning Status
   *
   * @param boolean $hasReturned
   *  False if this is the first time a user has been served this Transaction,
   *  True if this Transaction has happened some time ago and its details are being reviewed.
   * @return $this
   */
  public function setHasReturned($hasReturned) {
    $this->returning = (boolean) $hasReturned;

    return $this;
  }

  /**
   * Get Transaction Returning Status
   *
   * @return boolean|null
   *  False if this is the first time a user has been served this Transaction,
   *  True if this Transaction has happened some time ago and its details are being reviewed.
   *  Null if not set.
   */
  public function getHasReturned() {
    return $this->returning;
  }

  /**
   * @param mixed $voucher_discount
   *  Numeric voucher discount price.
   * @return $this
   */
  public function setVoucherDiscount($voucher_discount) {
    $this->voucher_discount = $this->validateNumericPrice($voucher_discount);

    return $this;
  }

  /**
   * @return string
   *  Numeric voucher discount price.
   */
  public function getVoucherDiscount() {
    return $this->voucher_discount;
  }

  /**
   * @param string $currency
   * @return $this
   */
  public function setCurrency($currency) {
    $this->currency = $currency;

    return $this;
  }

  /**
   * @return string
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @param string $shipping_method
   * @return $this
   */
  public function setShippingMethod($shipping_method) {
    $this->shipping_method = $shipping_method;

    return $this;
  }

  /**
   * @return string
   */
  public function getShippingMethod() {
    return $this->shipping_method;
  }

  /**
   * @param string $voucher
   * @return $this
   */
  public function setVoucher($voucher) {
    $this->voucher = $voucher;

    return $this;
  }

  /**
   * @return string
   */
  public function getVoucher() {
    return $this->voucher;
  }

  /**
   * @param string $payment_type
   *  Payment method, e.g. 'Visa', 'PayPal', 'Voucher'
   * @return $this
   */
  public function setPaymentType($payment_type) {
    $this->payment_type = $payment_type;

    return $this;
  }

  /**
   * @return string
   *  Payment method, e.g. 'Visa', 'PayPal', 'Voucher'
   */
  public function getPaymentType() {
    return $this->payment_type;
  }

  /**
   * @param string $order_id
   * @return $this
   */
  public function setOrderId($order_id) {
    $this->order_id = $order_id;

    return $this;
  }

  /**
   * @return string
   */
  public function getOrderId() {
    return $this->order_id;
  }

  /**
   * @param mixed $subtotal
   *  Numeric sub-total price
   * @return $this
   */
  public function setSubtotal($subtotal) {
    $this->subtotal = $this->validateNumericPrice($subtotal);

    return $this;
  }

  /**
   * @return string
   *  Numeric sub-total price
   */
  public function getSubtotal() {
    return $this->subtotal;
  }

  /**
   * @param QubitAddress $delivery
   * @return $this
   */
  public function setDeliveryAddress(QubitAddress $delivery) {
    $this->delivery = $delivery;

    return $this;
  }

  /**
   * @return QubitAddress
   */
  public function getDeliveryAddress() {
    return $this->delivery;
  }

  /**
   * @param QubitAddress $billing
   * @return $this
   */
  public function setBillingAddress(QubitAddress $billing) {
    $this->billing = $billing;

    return $this;
  }

  /**
   * @return QubitAddress
   */
  public function getBillingAddress() {
    return $this->billing;
  }

  /**
   * @param QubitLineItem[] $line_items
   * @return $this
   */
  public function setLineItems(array $line_items) {
    $this->line_items = $line_items;

    return $this;
  }

  /**
   * @return QubitLineItem[]
   */
  public function getLineItems() {
    return $this->line_items;
  }

}