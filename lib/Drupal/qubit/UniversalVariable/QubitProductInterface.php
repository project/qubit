<?php
/**
 * @file
 * Interface for the Universal Variable Product
 *
 * @package qubit
 */

namespace Drupal\qubit\UniversalVariable;

use Drupal\qubit\UniversalVariable\Product\QubitReview;
use Drupal\qubit\UniversalVariable\Product\QubitJourney;
use Drupal\qubit\UniversalVariable\Product\QubitAccommodation;

/**
 * Class ProductInterface
 *
 * @package Drupal\qubit\QubitUniversalVariable
 */
interface QubitProductInterface {

  /**
   * @param QubitProduct[] $linked_products
   * @return $this
   */
  public function setLinkedProducts(array $products);

  /**
   * @return QubitProduct[]|null
   */
  public function getLinkedProducts();

  /**
   * @param QubitJourney[] $journeys
   * @return $this
   */
  public function setJourneys(array $journeys);

  /**
   * @return QubitJourney[]|null
   */
  public function getJourneys();

  public function setAccommodations(array $accommodations);

  public function getAccommodations();

  /**
   * @param QubitReview[] $reviews
   * @return $this
   */
  public function setReviews(array $reviews);

  /**
   * @return QubitReview[]|null
   */
  public function getReviews();

  }