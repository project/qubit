<?php
/**
 * @file
 * Universal Variable
 *
 * The intention of the Universal Variable is to provide session and content information in a machine-readable format.
 *
 * @link https://github.com/QubitProducts/UniversalVariable
 *
 * @package qubit
 */

namespace Drupal\qubit;

use Drupal\qubit\UniversalVariable\QubitBasket;
use Drupal\qubit\UniversalVariable\QubitEvent;
use Drupal\qubit\UniversalVariable\QubitListing;
use Drupal\qubit\UniversalVariable\QubitPage;
use Drupal\qubit\UniversalVariable\QubitProduct;
use Drupal\qubit\UniversalVariable\QubitRecommendation;
use Drupal\qubit\UniversalVariable\QubitTransaction;
use Drupal\qubit\UniversalVariable\QubitUser;
use InvalidArgumentException;

/**
 * Class QubitUniversalVariable
 */
class QubitUniversalVariable extends AbstractUniversalVariable implements QubitUniversalVariableInterface {

  /**
   * The version of the Universal Variable Specification being used.
   *
   * @link https://github.com/QubitProducts/UniversalVariable#version
   */
  const version = "1.1.1";

  /** @var QubitUser */
  private $user;
  /** @var QubitPage */
  private $page;
  /** @var QubitProduct */
  private $product;
  /** @var QubitBasket */
  private $basket;
  /** @var QubitTransaction */
  private $transaction;
  /** @var QubitListing */
  private $listing;
  /** @var QubitRecommendation */
  private $recommendation;
  /** @var array */
  private $events;

  /**
   * @param string $name
   */
  public function __unset($name) {
    if (property_exists($this, $name)) {
      unset($this->{$name});
    }
  }

  /**
   * Get set properties
   *
   * @return array
   *  Set property values from the object keyed by property name.
   */
  protected function getSetProperties() {
    $all_properties = $this->getAllProperties();
    $set_properties = array_filter(
      $all_properties, function ($value) {
        return isset($value);
      }
    );

    return $set_properties;
  }

  /**
   * Get all properties
   *
   * @return array
   *  All property values from the object keyed by property name.
   */
  protected function getAllProperties() {
    $properties = get_object_vars($this);

    $all_properties = array();
    while (list ($full_name, $value) = each($properties)) {
      $full_name_components = explode("\0", $full_name);
      $property_name = array_pop($full_name_components);
      if ($property_name) {
        $all_properties[$property_name] = $value;
      }
    }

    return $all_properties;
  }

  /**
   * @param QubitPage $page
   * @return $this
   */
  public function setPage(QubitPage $page) {
    $this->page = $page;

    return $this;
  }

  /**
   * @return QubitPage
   */
  public function getPage() {
    return $this->page;
  }

  /**
   * @param QubitProduct $product
   * @return $this
   */
  public function setProduct(QubitProduct $product) {
    $this->product = $product;

    return $this;
  }

  /**
   * @return QubitProduct
   */
  public function getProduct() {
    return $this->product;
  }

  /**
   * @param QubitUser $user
   * @return $this
   */
  public function setUser(QubitUser $user) {
    $this->user = $user;

    return $this;
  }

  /**
   * @return QubitUser
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * @return QubitEvent[]|null
   */
  public function getEvents() {
    return $this->events;
  }

  /**
   * @param QubitEvent[] $events
   *  Contains the events that have occurred on the page.
   * @throws InvalidArgumentException
   * @return $this
   */
  public function setEvents(array $events) {
    foreach ($events as $item) {
      if (FALSE === $item instanceof QubitEvent) {
        throw new InvalidArgumentException("Type Error: events can only contain instances of class 'Event'.");
      }
    }

    $this->events = $events;

    return $this;
  }

  /**
   * @return $this
   */
  public function unsetEvents() {
    $this->events = NULL;

    return $this;
  }

  /**
   * @return string
   */
  public function getVersion() {
    return self::version;
  }

  /**
   * @param QubitBasket $basket
   * @return $this
   */
  public function setBasket(QubitBasket $basket) {
    $this->basket = $basket;

    return $this;
  }

  /**
   * @return QubitBasket
   */
  public function getBasket() {
    return $this->basket;
  }

  /**
   * @param QubitTransaction $transaction
   * @return $this
   */
  public function setTransaction(QubitTransaction $transaction) {
    $this->transaction = $transaction;

    return $this;
  }

  /**
   * @return QubitTransaction
   */
  public function getTransaction() {
    return $this->transaction;
  }

  /**
   * @param QubitListing $listing
   * @return $this
   */
  public function setListing(QubitListing $listing) {
    $this->listing = $listing;

    return $this;
  }

  /**
   * @return QubitListing
   */
  public function getListing() {
    return $this->listing;
  }

  /**
   * @param QubitRecommendation $recommendation
   * @return $this
   */
  public function setRecommendation(QubitRecommendation $recommendation) {
    $this->recommendation = $recommendation;

    return $this;
  }

  /**
   * @return QubitRecommendation
   */
  public function getRecommendation() {
    return $this->recommendation;
  }

}
