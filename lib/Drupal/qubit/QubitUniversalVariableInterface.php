<?php
/**
 * @file
 * Interface for the Universal Variable
 *
 * @package qubit
 */

namespace Drupal\qubit;

use Drupal\qubit\UniversalVariable\QubitBasket;
use Drupal\qubit\UniversalVariable\QubitListing;
use Drupal\qubit\UniversalVariable\QubitPage;
use Drupal\qubit\UniversalVariable\QubitProduct;
use Drupal\qubit\UniversalVariable\QubitRecommendation;
use Drupal\qubit\UniversalVariable\QubitTransaction;
use Drupal\qubit\UniversalVariable\QubitUser;

interface QubitUniversalVariableInterface {

  public function getVersion();

  public function getUser();

  public function setUser(QubitUser $user);

  public function getPage();

  public function setPage(QubitPage $page);

  public function getProduct();

  public function setProduct(QubitProduct $product);

  public function getBasket();

  public function setBasket(QubitBasket $basket);

  public function getTransaction();

  public function setTransaction(QubitTransaction $transaction);

  public function getListing();

  public function setListing(QubitListing $listing);

  public function getRecommendation();

  public function setRecommendation(QubitRecommendation $recommendation);

  public function getEvents();

  public function setEvents(array $events);

  public function unsetEvents();

}