<?php
/**
 * @file
 * This file contains code to emulate xautoload-style PSR-0 autoloading.
 *
 * Include this file to autoload Qubit class libraries in "./lib/Drupal".
 */


/**
 * Register a dedicated class loader only for this module.
 *
 * Call to initialize xautoloading for a specific module.
 */
function _qubit_include() {
  static $first_run = TRUE;
  if (!$first_run) {
    return;
  }
  $first_run = FALSE;
  if (!function_exists('module_exists') || !module_exists('xautoload')) {
    spl_autoload_register('_qubit_autoload');
  }
}


/**
 * The autoload callback for this specific module, used if xautoload is not
 * present.
 *
 * @param string $class
 *   The class we want to load.
 */
function _qubit_autoload($class) {

  static $lib_dir;
  if (!isset($lib_dir)) {
    $lib_dir = dirname(__FILE__) . '/lib/';
  }

  $module = 'qubit';
  $l = strlen($module);

  if (FALSE !== $nspos = strrpos($class, '\\')) {
    // PSR-0 mode.
    if ("Drupal\\$module\\" === substr($class, 0, $l + 8)) {
      $namespace = substr($class, 0, $nspos);
      $classname = substr($class, $nspos + 1);
      $path = $lib_dir . str_replace('\\', '/', $namespace) . '/' . str_replace('_', '/', $classname) . '.php';
      if (is_file($path)) {
        include $path;
      }
    }
  }
}

_qubit_include();
