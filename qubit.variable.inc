<?php
/**
 * @file
 * Variables defined by Qubit module.
 */

/**
 * Implements hook_variable_info().
 */
function qubit_variable_info($options) {
  $variable = array();

  $variable['qubit_opentag_url'] = array(
    'type' => 'url',
    'title' => t('OpenTag URL', array(), $options),
    'default' => '//d3c3cq33003psk.cloudfront.net/opentag-1-704958.js',
    'description' => t(
      'The URL of the OpenTag container JavaScript library to ues. See the <a href="!docurl">Qubit Universal Variable implementation guide</a> for more information.',
      array('!docurl' => 'http://docs.qubitproducts.com/uv/implement/'),
      $options
    ),
    'validate callback' => 'qubit_variable_opentag_url_validate',
    'multidomain' => TRUE,
    'required' => TRUE,
    'group' => 'qubit_settings',
  );

  $variable['qubit_opentag_enable'] = array(
    'type' => 'enable',
    'title' => t('Output the Opentag container', array(), $options),
    'default' => 1,
    'description' => t(
      'When output of the Qubit Opentag container is disabled the Universal Variable will still be populated but it will not be sent to Qubit.',
      array(), $options
    ),
    'multidomain' => TRUE,
    'group' => 'qubit_settings',
  );

  return $variable;
}

function qubit_variable_group_info() {
  $groups = array();

  $groups['qubit_settings'] = array(
    'title' => t('Qubit settings'),
    'description' => t('Qubit Opentag and Universal Variable settings.'),
    'access' => 'administer site configuration',
    'path' => array(
      'admin/config/services/qubit',
    ),
  );

  return $groups;
}
