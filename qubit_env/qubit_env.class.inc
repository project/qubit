<?php
/**
 * @file
 * Variable Realm controller.
 */

/**
 * @class Exception class used to throw error if a module dependency fails.
 */
class QubitEnvironmentDependencyException extends Exception { }

/**
 * Controller for Qubit Environment realms.
 */
class QubitEnvironmentVariableRealm extends VariableRealmDefaultController {
  /**
   * Implementation of VariableRealmControllerInterface::getDefaultKey().
   *
   * Get the Default Realm Key to use when no specific realm key is set.
   *
   * @throws QubitEnvironmentDependencyException
   *
   * @return string
   *   The default realm key.
   */
  public function getDefaultKey() {
    if (FALSE === defined('QUBIT_ENVIRONMENT_DEFAULT_REALM_KEY')) {
      throw new \QubitEnvironmentDependencyException(
        "Required dependency on Drupal module was not found. Tried to reference 'QUBIT_ENVIRONMENT_DEFAULT_REALM_KEY'."
      );
    }

    return QUBIT_ENVIRONMENT_DEFAULT_REALM_KEY;
  }

  /**
   * Implementation of VariableRealmControllerInterface::getRequestKey().
   *
   * Get the Realm Key that will be used for the current request.
   *
   * @return string
   *   The realm key to use for the current request.
   */
  public function getRequestKey() {
    $key = $this->getDefaultKey();

    foreach ($this->getAllKeys() as $name => $title) {
      $variable_name = "qubit_env_domain_{$name}";
      $env_domain_mask = variable_get($variable_name, NULL);
      if ($this->isMatchingHost($env_domain_mask)) {
        $key = $name;
        break;
      }
    }

    return $key;
  }

  /**
   * Is the mask a match for the host.
   *
   * Checks the given host name, or current server host if none given, to see
   * if it matches the given mask. The mask can be a sub-domain or an IP
   * sub-net.
   *
   * For example:
   * @code
   * // Host sub-domain match.
   * $mask = 'example.org';
   * $host = 'www.example.org';
   *
   * // IP sub-net match.
   * $mask = '192.168';
   * $host = '192.168.0.2';
   * @endcode
   *
   * @param string $mask
   *   The mask to match against the host. For domain names a sub-domain can be
   *   used. For IP addresses a sub-net can be used.
   * @param string|null $host
   *   Optional host name or IP address to match against. The current server
   *   host is used if none given.
   *
   * @return bool
   */
  private function isMatchingHost($mask, $host = NULL) {
    if (!is_string($mask) || empty($mask)) {

      return FALSE;
    }
    if (!is_string($host) || empty($host)) {
      $host = $this->getServerHostName();
    }
    if (!$host) {

      return FALSE;
    }
    $domain_parts = explode(
      '.', implode('.', array_reverse(explode(':', rtrim($host, '.'))))
    );
    $mask = trim($mask, '.');

    $is_ip = filter_var($host, FILTER_VALIDATE_IP);
    if ($is_ip) {
      $domain_parts = array_reverse($domain_parts);
    }

    for ($j = count($domain_parts); $j > 0; $j--) {
      $subdomain_parts = array_slice($domain_parts, -$j);
      if ($is_ip) {
        $subdomain_parts = array_reverse($subdomain_parts);
      }
      $subdomain = implode('.', $subdomain_parts);
      if ($subdomain == $mask) {

        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Implementation of VariableRealmControllerInterface::getAllKeys().
   *
   * @throws QubitEnvironmentDependencyException
   *
   * @return array
   *   An associative array of available Realms keys and their respective
   *   human readable names.
   */
  public function getAllKeys() {
    if (FALSE === function_exists('_qubit_env_get_options')) {
      throw new \QubitEnvironmentDependencyException(
        "Required dependency on Drupal module was not found. Tried to call '_qubit_env_get_options'."
      );
    }

    return _qubit_env_get_options();
  }

  /**
   * Get a host name for the server.
   *
   * Tries to get the requested host name, then uses the IP address if the host
   * is unavailable.
   *
   * @return string|null
   *   The server host name else IP address else null.
   */
  private function getServerHostName() {
    foreach (array('HTTP_HOST', 'SERVER_ADDR', 'LOCAL_ADDR') as $name_type) {
      if (array_key_exists($name_type, $_SERVER)
          && is_string($_SERVER[$name_type])
          && !empty($_SERVER[$name_type])
      ) {

        return $_SERVER[$name_type];
      }
    }

    return NULL;
  }

}
